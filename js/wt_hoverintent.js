(function ($, Drupal, drupalSettings, once) {
  'use strict';

  Drupal.behaviors.wtOpenMenuOnHover = Drupal.behaviors.wtOpenMenuOnHover || {
    attach: function (context, settings) {
      let $elements = $(once('wtOpenMenuOnHover', '.nav__a', context));
      $elements.each(function () {
        let ul = this.nextElementSibling;
        if (ul && ul.tagName == 'UL') {
          hoverintent(
            ul.parentNode,
            function () {
              let computedStyles = getComputedStyle(ul);
              let hover = (computedStyles.getPropertyValue('--menu--open-on-hover') || 'false').trim() == 'true';
              let displayNone = computedStyles.getPropertyValue('display') == 'none';
              let animation = (computedStyles.getPropertyValue('--menu--animation') || 'collapse').trim();
              if (hover && displayNone) {
                drupalSettings.wt.menu[animation](ul);
              }
            },
            function () {
              let computedStyles = getComputedStyle(ul);
              let hover = (computedStyles.getPropertyValue('--menu--open-on-hover') || 'false').trim() == 'true';
              let displayNone = computedStyles.getPropertyValue('display') == 'none';
              let animation = (computedStyles.getPropertyValue('--menu--animation') || 'collapse').trim();
              if (hover && !displayNone) {
                drupalSettings.wt.menu[animation](ul);
              }
            }
          );
        }
      });
    }
  }

} (jQuery, Drupal, drupalSettings, once));
