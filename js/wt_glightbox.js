(function ($, Drupal, drupalSettings, once) {
  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.lightbox = drupalSettings.wt.lightbox || {};


  Drupal.behaviors.wtLightbox = Drupal.behaviors.wtLightbox || {
    attach: function (context, settings) {
      let $elements = $(once('wtLightbox', document, context));
      $elements.each( function() {
        drupalSettings.wt.lightbox.global = GLightbox();
      });
    }
  }

}(jQuery, Drupal, drupalSettings, once));
