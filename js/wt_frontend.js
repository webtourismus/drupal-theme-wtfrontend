(function ($, Drupal, drupalSettings, once) {
  'use strict';
  Drupal.wt = Drupal.wt || {};
  drupalSettings.wt = drupalSettings.wt || {};

  Drupal.wt.moveLinksFromLocalTasksToToolbar = Drupal.wt.moveLinksFromLocalTasksToToolbar || function(toolbar, localTasks) {
    let editLink = localTasks.querySelector('a[data-drupal-link-system-path^="node/"][data-drupal-link-system-path$="/edit"], a[data-drupal-link-system-path="admin/lunch/week"]');
    if (editLink) {
      toolbar.insertAdjacentHTML('beforeend', '<div class="toolbar-tab"><a href="' + editLink.getAttribute('href') + '" class="toolbar-icon toolbar-icon-tablink-edit toolbar-icon-tablink toolbar-item"><i class="fas fa-edit"></i>' + editLink.innerHTML + '</a></div>');
      editLink.parentElement.removeChild(editLink);

    }
    let layoutLink = localTasks.querySelector('a[data-drupal-link-system-path^="node/"][data-drupal-link-system-path$="/layout"]');
    if (layoutLink) {
      toolbar.insertAdjacentHTML('beforeend', '<div class="toolbar-tab"><a href="' + layoutLink.getAttribute('href') + '" class="toolbar-icon toolbar-item"><i class="fas fa-th-large"></i>' + layoutLink.innerHTML + '</a></div>');
      layoutLink.parentElement.removeChild(layoutLink);
    }
    let translateLink = localTasks.querySelector('a[data-drupal-link-system-path^="node/"][data-drupal-link-system-path$="/translations"]');
    if (translateLink) {
      toolbar.insertAdjacentHTML('beforeend', '<div class="toolbar-tab"><a href="' + translateLink.getAttribute('href') + '" class="toolbar-icon toolbar-item"><i class="fas fa-language"></i>' + translateLink.innerHTML + '</a></div>');
      translateLink.parentElement.removeChild(translateLink);
    }
    let previewLink = document.querySelector('.node-preview-container a.node-preview-backlink');
    if (previewLink) {
      toolbar.insertAdjacentHTML('beforeend', '<div class="toolbar-tab"><a href="' + previewLink.getAttribute('href') + '" class="toolbar-icon toolbar-item"><i class="fas fa-arrow-alt-left"></i>' + previewLink.innerHTML + '</a></div>');
      previewLink.parentElement.removeChild(previewLink);
    }
  }

  /**
   * When toolbar loads last, fetch links from local tasks
   */
  Drupal.behaviors.wtToolbarFetchLocalTasks = Drupal.behaviors.wtToolbarFetchLocalTasks || {
    attach: function (context, settings) {
      let $elements = $(once('wtToolbarFetchLocalTasks', '#toolbar-bar', context));
      $elements.each(function () {
        let toolbar = this;
        let localTasks = document.getElementById('block-localtask');
        if (toolbar && localTasks) {
          Drupal.wt.moveLinksFromLocalTasksToToolbar(toolbar, localTasks)
        }
      });
    }
  };
  /**
   * When local tasks loads last, push links to toolbar
   */
  Drupal.behaviors.wtLocalTasksPushToToolbar = Drupal.behaviors.wtLocalTasksPushToToolbar || {
    attach: function (context, settings) {
      let $elements = $(once('wtLocalTasksPushToToolbar', '#block-localtask', context));
      $elements.each(function () {
        let toolbar = document.getElementById('toolbar-bar');
        let localTasks = this;
        if (toolbar && localTasks) {
          Drupal.wt.moveLinksFromLocalTasksToToolbar(toolbar, localTasks)
        }
      });
    }
  };


  /**
   * the "real", always correct, current viewport height, even on mobile!
   * use this if you need 100vh no matter if address bar is visible or not on a mobile browser
   *
   * .some_element { height: var(--viewport-height); }
   */
  Drupal.wt.setViewportHeightProperty = Drupal.wt.setViewportHeightProperty || function () {
    document.documentElement.style.setProperty('--viewport-height', window.innerHeight + 'px');
  }

  Drupal.behaviors.wtSetViewportHeight = Drupal.behaviors.wtSetViewportHeight || {
    attach: function (context, settings) {
      let $elements = $(once('wtSetViewportHeight', document, context));
      $elements.each(function () {
        Drupal.wt.setViewportHeightProperty();
        window.addEventListener('orientationchange', Drupal.wt.setViewportHeightProperty);
        window.addEventListener('scroll', Drupal.wt.setViewportHeightProperty);
        window.addEventListener('resize', Drupal.wt.setViewportHeightProperty);
      });
    }
  }

  /**
   * Helper function to return the height in pixels of all fixed elements
   *  on top of a page as float number.
   *  @see config.scss
   * .wt__header { --fixed-top: calc(var(--toolbar--height) + var(--header--height)); }
   *
   * For use with JS functions, e.g. in-page scroll links <a href="#scroll=.somewhere">
   */
  Drupal.wt.getFixedTop = Drupal.wt.getFixedTop || function () {
    let possiblyNotAPixelValue = getComputedStyle(document.querySelector('.wt__header')).getPropertyValue('--fixed-top').trim();
    let helperElement = document.querySelector('.region--invisible').appendChild(document.createElement('div'));
    helperElement.style.width = possiblyNotAPixelValue;
    let calcualtedPixelValue = getComputedStyle(helperElement).getPropertyValue('width').replace(/px/, '');
    document.querySelector('.region--invisible').removeChild(helperElement);
    return calcualtedPixelValue;
  }

  Drupal.behaviors.wtBacklinks = Drupal.behaviors.wtBacklinks || {
    attach: function (context, settings) {
      let $elements = $(once('wtBacklinks', '.js-backlink, [href*="#js-backlink"]', context));
      $elements.each(function () {
        $(this).attr('href', document.referrer);
        $(this).on('click', function() {
          history.back();
          return false;
        });
      });
    }
  };

  Drupal.behaviors.wtLinkblockImageAsMoreLink = Drupal.behaviors.wtLinkblockImageAsMoreLink || {
    attach: function (context, settings) {
      let $elements = $(once('wtLinkblockImageAsMoreLink', '.linkblock__imgcanvas--linked', context));
      $elements.each(function () {
        $(this).on('click', function(event) {
          if ($(event.target).hasClass('glide__arrow') || $(event.target).parents('.glide__arrow').length >= 1) {
            return false;
          }
          let $possibleLink = $(this).parents('.linkblock').find('.linkblock__title a');
          if ($possibleLink.length == 1) {
            $possibleLink.get(0).click();
          }
        });
      });
    }
  };

  Drupal.wt.toggle = Drupal.wt.toggle || {};
  Drupal.wt.toggle.options = Drupal.wt.toggle.options || {};
  Drupal.wt.toggle.options.duration = Drupal.wt.toggle.options.duration ?? 400;

  /**
   * Simple visibility toggle that only switches classes from
   * .is-collapsed => .is-unstable.is-collapsed => .is-expanded =>
   * .is-unstable.is-expanded => .is-collapsed for the element to be animated
   *
   * corresponding CSS styles are required in the SASS files
   *
   * dynamic initial values can be set with CSS media queries instead of fixed classes
   * e.g. @media (...) { .target { --js--toggle-state: is-collapsed } }
   */
  Drupal.wt.toggle.toggle = Drupal.wt.toggle.toggle || function (element) {
    let source = element;
    let target = document.querySelector(source.getAttribute('data-target'));

    if (!target.classList.contains('is-unstable')) {
      target.classList.add('is-unstable');

      let options = Drupal.wt.toggle.options;
      if (source.getAttribute('data-animation-duration') == parseInt(source.getAttribute('data-animation-duration'), 10)) {
        options.duration = source.getAttribute('data-animation-duration');
      }

      if (target.classList.contains('is-expanded') || getComputedStyle(target).getPropertyValue('--js--toggle-state').trim() == 'is-expanded') {
        window.setTimeout(function () {
          source.setAttribute('aria-expanded', 'false');
          target.classList.remove('is-unstable');
          target.classList.remove('is-expanded');
          target.classList.add('is-collapsed');
          target.style.setProperty('--js--toggle-state', 'is-collapsed');
        }, options.duration);
      }
      else {
        window.setTimeout(function () {
          source.setAttribute('aria-expanded', 'true');
          target.classList.remove('is-unstable');
          target.classList.remove('is-collapsed');
          target.classList.add('is-expanded');
          target.style.setProperty('--js--toggle-state', 'is-expanded');
        }, options.duration);
      }
    }
  }


  /**
   * Toogle visibility using jQuery's fadeIn/fadeOut functions. Useful to
   * animate to "height: auto" which cannot be done with CSS
   */
  Drupal.wt.toggle.fade = Drupal.wt.toggle.fade || function (element) {
    let source = element;
    let target = document.querySelector(source.getAttribute('data-target'));

    if (!target.classList.contains('is-unstable')) {
      target.classList.add('is-unstable');

      let options = Drupal.wt.toggle.options;
      if (source.getAttribute('data-animation-duration') == parseInt(source.getAttribute('data-animation-duration'), 10)) {
        options.duration = source.getAttribute('data-animation-duration');
      }

      let $target = $(target);
      if (target.classList.contains('is-expanded') || getComputedStyle(target).getPropertyValue('--js--toggle-state').trim() == 'is-expanded') {
        options.complete = function () {
          source.setAttribute('aria-expanded', 'false');
          target.classList.remove('is-unstable');
          target.classList.remove('is-expanded');
          target.classList.add('is-collapsed');
          target.style.setProperty('--js--toggle-state', 'is-collapsed');
        }
        $target['fadeOut'](options);
      }
      else {
        if (source.getAttribute('data-toggle-to-display')) {
          //by default jQuery would animate to "display: block", the following
          //line will tells jQuery to animate to some other CSS display setting
          $target.css('display', source.getAttribute('data-toggle-to-display')).hide();
          source.removeAttribute('data-toggle-to-display');
        }
        options.complete = function () {
          source.setAttribute('aria-expanded', 'true');
          target.classList.remove('is-unstable');
          target.classList.remove('is-collapsed');
          target.classList.add('is-expanded');
          target.style.setProperty('--js--toggle-state', 'is-expanded');
        }
        $(target)['fadeIn'](options);
      }
    }
  }

  /**
   * Toogle visibility using jQuery's slideDown/slideUp functions. Useful to
   * animate to "height: auto" which cannot be done with CSS
   */
  Drupal.wt.toggle.collapse = Drupal.wt.toggle.collapse || function (element) {
    let source = element;
    let target = document.querySelector(source.getAttribute('data-target'));

    if (!target.classList.contains('is-unstable')) {
      target.classList.add('is-unstable');

      let options = Drupal.wt.toggle.options;
      if (source.getAttribute('data-animation-duration') == parseInt(source.getAttribute('data-animation-duration'), 10)) {
        options.duration = source.getAttribute('data-animation-duration');
      }

      let $target = $(target);
      if (target.classList.contains('is-expanded') || getComputedStyle(target).getPropertyValue('--js--toggle-state').trim() == 'is-expanded') {
        options.complete = function () {
          source.setAttribute('aria-expanded', 'false');
          target.classList.remove('is-unstable');
          target.classList.remove('is-expanded');
          target.classList.add('is-collapsed');
          target.style.setProperty('--js--toggle-state', 'is-collapsed');
        }
        $target['slideUp'](options);
      }
      else {
        if (source.getAttribute('data-toggle-to-display')) {
          //by default jQuery would animate to "display: block", the following
          //line will tells jQuery to animate to some other CSS display setting
          $target.css('display', source.getAttribute('data-toggle-to-display')).hide();
          source.removeAttribute('data-toggle-to-display');
        }
        options.complete = function () {
          source.setAttribute('aria-expanded', 'true');
          target.classList.remove('is-unstable');
          target.classList.remove('is-collapsed');
          target.classList.add('is-expanded');
          target.style.setProperty('--js--toggle-state', 'is-expanded');
        }
        $(target)['slideDown'](options);
      }
    }
  }

  /**
   * Toogle visibility using jQuery's animate function. Useful to animate
   * to "width: auto" which cannot be done with CSS
   */
  Drupal.wt.toggle.slide = Drupal.wt.toggle.slide || function (element) {
    let source = element;
    let target = document.querySelector(source.getAttribute('data-target'));

    if (!target.classList.contains('is-unstable')) {
      target.classList.add('is-unstable');

      let options = Drupal.wt.toggle.options;
      if (source.getAttribute('data-animation-duration') == parseInt(source.getAttribute('data-animation-duration'), 10)) {
        options.duration = source.getAttribute('data-animation-duration');
      }

      let $target = $(target);
      if (target.classList.contains('is-expanded') || getComputedStyle(target).getPropertyValue('--js--toggle-state').trim() == 'is-expanded') {
        options.complete = function () {
          source.setAttribute('aria-expanded', 'false');
          target.classList.remove('is-unstable');
          target.classList.remove('is-expanded');
          target.classList.add('is-collapsed');
          target.style.setProperty('--js--toggle-state', 'is-collapsed');
        }
        $target['animate']({
          'width': 'toggle',
          'padding': 'toggle',
          'border': 'toggle',
          'margin': 'toggle'
        }, options);
      }
      else {
        if (source.getAttribute('data-toggle-to-display')) {
          //by default jQuery would animate to "display: block", the following
          //line will tells jQuery to animate to some other CSS display setting
          $target.css('display', source.getAttribute('data-toggle-to-display')).hide();
          source.removeAttribute('data-toggle-to-display');
        }
        options.complete = function () {
          source.setAttribute('aria-expanded', 'true');
          target.classList.remove('is-unstable');
          target.classList.remove('is-collapsed');
          target.classList.add('is-expanded');
          target.style.setProperty('--js--toggle-state', 'is-expanded');
        }
        $target['animate']({
          'width': 'toggle',
          'padding': 'toggle',
          'border': 'toggle',
          'margin': 'toggle'
        }, options);
      }
    }
  }

  Drupal.behaviors.wtToggleVisibility = Drupal.behaviors.wtToggleVisibility || {
    attach: function (context, settings) {
      let $elements = $(once('wtToggleVisibility', '[data-toggle]', context));
      $elements.each(function () {
        $(this).on('click', function (event) {
          let toggleFunction = event.currentTarget.getAttribute('data-toggle');
          Drupal.wt.toggle[toggleFunction](event.currentTarget);
          event.preventDefault();
          event.stopPropagation();
        });
      });
    }
  }

  Drupal.wt.menu = Drupal.wt.menu || {};

  /**
   * Toogle mainnav using jQuery's fadeIn/fadeOut functions. Useful to
   * animate to "height: auto" which cannot be done with CSS
   */
  Drupal.wt.menu.fade = Drupal.wt.menu.fade || function (ul) {
    if (!ul.classList.contains('is-unstable')) {
      ul.classList.add('is-unstable');
      let display = getComputedStyle(ul).getPropertyValue('display') || 'none';
      let $ul = $(ul);
      if (display == 'none') {
        $ul.stop(true, true).fadeIn({
          'complete': function () {
            ul.classList.remove('is-collapsed');
            ul.classList.remove('is-unstable');
            ul.classList.add('is-expanded');
          }
        });
      }
      else {
        $ul.stop(true, true).fadeOut({
          'complete': function () {
            ul.classList.remove('is-expanded');
            ul.classList.remove('is-unstable');
            ul.classList.add('is-collapsed');
          }
        });
      }
    }
  }

  /**
   * Toggle mainnav using jQuery's slideDown/slideUp functions. Useful to
   * animate to "height: auto" which cannot be done with CSS
   */
  Drupal.wt.menu.collapse = Drupal.wt.menu.collapse || function (ul) {
    if (!ul.classList.contains('is-unstable')) {
      ul.classList.add('is-unstable');
      let display = getComputedStyle(ul).getPropertyValue('display') || 'none';
      let $ul = $(ul);
      if (display === 'none') {
        $ul.stop(true, true).slideDown({
          'complete': function () {
            ul.classList.remove('is-collapsed');
            ul.classList.remove('is-unstable');
            ul.classList.add('is-expanded');
          }
        });
      }
      else {
        $ul.stop(true, true).slideUp({
          'complete': function () {
            ul.classList.remove('is-expanded');
            ul.classList.remove('is-unstable');
            ul.classList.add('is-collapsed');
          }
        });
      }
    }
  }

  /**
   * Toggle mainnav using jQuery's animate function. Useful to animate
   * to "width: auto" which cannot be done with CSS
   */
  Drupal.wt.menu.slide = Drupal.wt.menu.slide || function (ul) {
    if (!ul.classList.contains('is-unstable')) {
      ul.classList.add('is-unstable');
      let display = getComputedStyle(ul).getPropertyValue('display') || 'none';
      let $ul = $(ul);
      if (display === 'none') {
        $ul.stop(true, true).animate(
          {
            'width': 'toggle',
            'padding': 'toggle',
            'margin': 'toggle',
            'border': 'toggle',
          },
          {
            'complete': function () {
              ul.classList.remove('is-collapsed');
              ul.classList.remove('is-unstable');
              ul.classList.add('is-expanded');
            }
          }
        );
      }
      else {
        $ul.stop(true, true).animate(
          {
            'width': 'toggle',
            'padding': 'toggle',
            'margin': 'toggle',
            'border': 'toggle',
          },
          {
            'complete': function () {
              ul.classList.remove('is-expanded');
              ul.classList.remove('is-unstable');
              ul.classList.add('is-collapsed');
            }
          }
        );
      }
    }
  }

  Drupal.behaviors.wtMenuToggle = Drupal.behaviors.wtMenuToggle || {
    attach: function (context, settings) {
      let $elements = $(once('wtMenuToggle', '.nav__a[href$="#toggle"]', context));
      $elements.each(function () {
        $(this).on('click', function (event) {
          let ul = this.nextElementSibling;
          if (ul && ul.tagName == 'UL') {
            let display = getComputedStyle(ul).getPropertyValue('display') || 'none';
            let toggleFunction = (getComputedStyle(ul).getPropertyValue('--menu--animation') || 'collapse').trim();
            if (display == 'none') {
              let allUls = this.parentNode.parentNode.querySelectorAll('.nav__ul');
              let needCloseUls = [...allUls].filter(elem => {
                return (getComputedStyle(elem).getPropertyValue('display') != 'none' && elem != ul);
              });
              needCloseUls.forEach(elem => Drupal.wt.menu[toggleFunction](elem));
            }
            Drupal.wt.menu[toggleFunction](ul);
          }
          event.preventDefault();
          event.stopPropagation();
        });
      });
    }
  }

  Drupal.wt.scrollTo = Drupal.wt.scrollTo || function (uri) {
    if ((uri.indexOf(location.pathname) === 0 || uri.indexOf('#scroll') === 0) && uri.indexOf('#scroll') !== -1) {
      let selector = decodeURIComponent(uri.substring(uri.indexOf('#scroll') + '#scroll'.length));
      let regex = /^=[#\.\[\]a-zA-Z0-9-_]+$/;
      if (selector.search(regex) === -1) {
        return false;
      }

      selector = selector.substring(1);
      let element = document.querySelector(selector);
      try {
        let fixedOffsetWithSal = Drupal.wt.getFixedTop();
        let sal = element.getAttribute('data-sal');
        if (sal && element.classList.contains('sal-animate')) {
          if (sal.indexOf('-down')) {
            fixedOffsetWithSal += 100;
          }
          else if (sal.indexOf('-up')) {
            fixedOffsetWithSal -= 100;
          }
        }
        let elementFromTop = Math.floor(element.getBoundingClientRect().top);
        console.info(elementFromTop - fixedOffsetWithSal);
        window.scrollBy({
          top: elementFromTop - fixedOffsetWithSal,
          left: 0,
          behavior: 'smooth'
        });
        dataLayer.push({
          'event': 'GAEvent',
          'eventCategory': 'scrollto',
          'eventAction': selector,
          'eventLabel': undefined,
          'eventNonInteraction': true
        });
      }
      catch (err) { /* fail silently */
      }
    }
  }

  Drupal.behaviors.wtScrollto = Drupal.behaviors.wtScrollto || {
    attach: function (context, settings) {
      let $elements = $(once('wtScrollto', 'a[href*="#scroll"]', context));
      $elements.each(function () {
        $(this).on('click', function (event) {
          Drupal.wt.scrollTo($(this).attr('href'));
        });
      });
    }
  }

  Drupal.behaviors.wtImagesLoaded = Drupal.behaviors.wtImagesLoaded || {
    attach: function (context, settings) {
      let $elements = $(once('wtImagesLoaded', document, context));
      $elements.each(function () {
        imagesLoaded(document.querySelector('body'), function () {
          window.setTimeout(Drupal.wt.scrollTo(location.pathname + location.hash), 200);
        })
      });
    }
  }

  Drupal.wt.findGALabel = Drupal.wt.findGALabel || function ($elem) {
    if ($elem.attr('data-ga-label')) {
      return $elem.attr('data-ga-label');
    }
    else if ($elem.parents('[data-ga-label]').length > 0) {
      return $elem.parents('[data-ga-label]').first().attr('data-ga-label');
    }
    else if ($elem.parents('.wt__main').length > 0) {
      return 'main';
    }
    else if ($elem.parents('.wt__header').length > 0) {
      return 'header';
    }
    else if ($elem.parents('.wt__footer').length > 0) {
      return 'footer';
    }
    else if ($elem.parents('.wt__aside').length > 0) {
      return 'aside';
    }
    else {
      return undefined;
    }
  }

  Drupal.behaviors.wtAddGAEvents = Drupal.behaviors.wtAddGAEvents || {
    attach: function (context, settings) {
      let $elements = $(once('wtAddGAEvents', 'a', context));
      $elements.on('click', function () {
        if (typeof dataLayer === 'undefined') {
          return;
        }
        let href = this.getAttribute('href').toLowerCase();
        /**
         * tel: links
         */
        if (href.lastIndexOf('tel:', 0) === 0) {
          dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'phone',
            'eventAction': $(this).attr('href').substring('tel:'.length),
            'eventLabel': Drupal.wt.findGALabel($(this)),
            'eventNonInteraction': false
          });
        }
        /**
         * mailto: links
         */
        else if (href.lastIndexOf('mailto:', 0) === 0) {
          dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'email',
            'eventAction': $(this).attr('href').substring('mailto:'.length),
            'eventLabel': Drupal.wt.findGALabel($(this)),
            'eventNonInteraction': false
          });
        }
        /**
         * external links
         */
        else if (href.lastIndexOf('http://', 0) === 0 || href.lastIndexOf('https://', 0) === 0) {
          if (RegExp('^https?://').test(href) && !RegExp(location.host).test(href) && !RegExp('(\.pdf|\.zip|\.docx?\.xlsx?)$', 'i').test(href)) {
            dataLayer.push({
              'event': 'GAEvent',
              'eventCategory': $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'outbound',
              'eventAction': href,
              'eventLabel': Drupal.wt.findGALabel($(this)),
              'eventNonInteraction': false
            });
          }
        }
        /**
         * common downloads
         */
        else if (href.indexOf('.pdf', href.length - 4) !== -1 || href.indexOf('.zip', href.length - 4) !== -1 || href.indexOf('.doc', href.length - 4) !== -1 || href.indexOf('.docx', href.length - 5) !== -1 || href.indexOf('.xls', href.length - 5) !== -1 || href.indexOf('.xlsx', href.length - 5) !== -1) {
          dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'download',
            'eventAction': $(this).attr('href'),
            'eventLabel': Drupal.wt.findGALabel($(this)),
            'eventNonInteraction': false
          });
        }
      });
      let $elements2 = $(once('wtAddGAEventsLightbox', '.glightbox', context));
      $elements2.on('click', function () {
        if (typeof dataLayer === 'undefined') {
          return;
        }
        dataLayer.push({
          'event': 'GAEvent',
          'eventCategory': $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'lightbox',
          'eventAction': $(this).attr('href'),
          'eventLabel': Drupal.wt.findGALabel($(this)),
          'eventNonInteraction': false
        });
      });
    }
  }


}(jQuery, Drupal, drupalSettings, once));
