(function ($, Drupal, drupalSettings, once) {
  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.scrollpos = drupalSettings.wt.scrollpos || {};
  drupalSettings.wt.scrollpos.options = drupalSettings.wt.scrollpos.options || {};
  drupalSettings.wt.scrollpos.options.spsClass = drupalSettings.wt.scrollpos.options.spsClass || 'wt__header';
  drupalSettings.wt.scrollpos.options.classAbove = drupalSettings.wt.scrollpos.options.classAbove || 'wt__header--above';
  drupalSettings.wt.scrollpos.options.classBelow = drupalSettings.wt.scrollpos.options.classBelow || 'wt__header--below';
  if (typeof drupalSettings.wt.scrollpos.options.scrollOffsetY === 'undefined') {
    drupalSettings.wt.scrollpos.options.scrollOffsetY = (function(){
      return document.querySelector('.wt__header').classList.contains('wt__header--banner') ? (document.querySelector('.wt__header').offsetHeight / 3) : 0;
    })();
  }


  Drupal.behaviors.wtScrollpos = Drupal.behaviors.wtScrollpos || {
    attach: function (context, settings) {
      let $elements = $(once('wtScrollpos', document, context));
      $elements.each( function() {
        ScrollPosStyler.init(drupalSettings.wt.scrollpos.options);
      });
    }
  }

}(jQuery, Drupal, drupalSettings, once));
