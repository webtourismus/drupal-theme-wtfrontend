(function ($, Drupal, drupalSettings, once) {
  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.glide = drupalSettings.wt.glide || {};
  drupalSettings.wt.glide.glide = drupalSettings.wt.glide.glide || [];
  drupalSettings.wt.glide.options = drupalSettings.wt.glide.options || {};
  drupalSettings.wt.glide.options.type = drupalSettings.wt.glide.options.type || 'carousel';
  drupalSettings.wt.glide.options.focusAt = drupalSettings.wt.glide.options.focusAt ?? 'center';
  drupalSettings.wt.glide.options.autoplay = drupalSettings.wt.isAdminTheme ? false : (drupalSettings.wt.glide.options.autoplay ?? false);
  drupalSettings.wt.glide.options.dragThreshold = drupalSettings.wt.isAdminTheme ? false : (drupalSettings.wt.glide.options.dragThreshold ?? 120);
  drupalSettings.wt.glide.options.swipeThreshold = drupalSettings.wt.isAdminTheme ? false : (drupalSettings.wt.glide.options.swipeThreshold ?? 80);

  Drupal.wt = Drupal.wt || {};
  Drupal.wt.glide = Drupal.wt.glide || {};
  Drupal.wt.glide.getResponsivePerViewOptions = Drupal.wt.glide.getResponsivePerViewOptions || function(slidesMinWidth = 1, trackWidth = 1) {
    if (!(slidesMinWidth > 1)) {
      return {perView: 1};
    }
    if (!(trackWidth > slidesMinWidth)) {
      return {perView: 1};
    }
    let maxSlides = Math.floor(trackWidth / slidesMinWidth);
    if (!(maxSlides > 1)) {
      return {perView: 1};
    }
    let responsivePerViewOptions = { perView: maxSlides, breakpoints: {} };
    for (let i=2; i<=maxSlides; i++) {
      let breakpoint = (i * slidesMinWidth) - 1;
      responsivePerViewOptions.breakpoints[breakpoint] = {
        perView: i-1
      }
    }
    return responsivePerViewOptions;
  };

  /**
   * Create carousels with glide JS
   */
  Drupal.behaviors.wtGlide = Drupal.behaviors.wtGlide || {
    attach: function (context, settings) {

      //simple single-image slider
      let $elements = $(once('wtGlide', '.glide--single', context));
      $elements.each( function() {
        let glideSettings = drupalSettings.wt.glide.options;
        if ($(this).attr('data-glide-options')) {
          $.extend(glideSettings, JSON.parse($(this).attr('data-glide-options')));
        }
        drupalSettings.wt.glide.glide.push( new Glide(this, glideSettings).mount() );
      });

      //dynamic gallery with multiple images per view
      let $elements2 = $(once('wtGlideGallery', '.glide--gallery', context));
      $elements2.each( function() {
        let glideSettings = drupalSettings.wt.glide.options;
        if ($(this).attr('data-glide-options')) {
          $.extend(glideSettings, JSON.parse($(this).attr('data-glide-options')));
        }
        let minWidth = $(this).attr('data-glide-slides-minwidth');
        let containerWidth = $(this).children('.glide__track').innerWidth();
        $.extend(glideSettings, Drupal.wt.glide.getResponsivePerViewOptions(minWidth, containerWidth));
        drupalSettings.wt.glide.glide.push( new Glide(this, glideSettings).mount() );
      });

      try {
        if (typeof drupalSettings.wt.lightbox.global.reload === 'function') {
          /* cloned glide slides with lightbox links require a re-init of the lightbox */
          drupalSettings.wt.lightbox.global.reload();
        }
      }
      catch (e) { /* fail silently if there is no lightbox to re-init */ }
    }
  }
} (jQuery, Drupal, drupalSettings, once));
